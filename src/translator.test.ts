import { Translator } from './translator';

describe('Translator defaults', () => {
  test('is available', () => {
    expect(Translator.getError('default')).toBe('An unspecified error occurred');
  });
});

describe('getError', () => {
  test('falls back to default if key is not found', () => {
    expect(Translator.getError('unknownKey')).toBe('An unspecified error occurred');
  });

  test("falls back to 'en-US' if locale not found", () => {
    Translator.addError('test', 'Test');

    expect(Translator.getError('test', 'af')).toBe('Test');
  });

  test('falls back to default if key and locale is not found', () => {
    expect(Translator.getError('unknownKey', 'af')).toBe('An unspecified error occurred');
  });
});

describe('addError', () => {
  test('creates locale if it does not exist', () => {
    Translator.addError('test', 'Toets %s', 'af');

    expect(Translator.getError('test', 'af')).toBe('Toets %s');
  });
});
