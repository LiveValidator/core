/**
 * Functor expected that each tester should adhere to
 * @param value The value this tester is testing
 * @param locale The locale to use for the errors
 * @returns A string or array of string with errors if there are any
 */
export type ITester = (value: any, locale?: string) => string | string[] | void;
