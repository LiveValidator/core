import { ITester } from './abstractions';
import { Core } from './core';

describe('Constructor', () => {
  let core: Core;

  // We want this to be possible so that users can add testers dynamically later
  test('with no testers to not fail', () => {
    const action = () => (core = new Core());

    expect(action).not.toThrow();
    expect(core).toBeDefined();
  });

  test('with one tester to succeed', () => {
    const action = () =>
      (core = new Core(_ => {
        return;
      }));

    expect(action).not.toThrow();
    expect(core).toBeDefined();
  });

  test('with more than one tester to succeed', () => {
    const action = () =>
      (core = new Core(
        _ => {
          return;
        },
        (_, locale) => {
          return locale;
        },
        _ => {
          return;
        },
      ));

    expect(action).not.toThrow();
    expect(core).toBeDefined();
  });
});

describe('validate', () => {
  test('passes when tester passes', () => {
    const core = new Core(_ => {
      return;
    });

    expect(core.validate('')).toBeTruthy();
  });

  test('fails when tester fails', () => {
    const core = new Core(_ => 'error');

    expect(core.validate('')).toBeFalsy();
  });

  test('fails when at least one tester fails', () => {
    const core = new Core(
      _ => {
        return;
      },
      _ => {
        return;
      },
      _ => 'error',
      _ => {
        return;
      },
      _ => {
        return;
      },
    );

    expect(core.validate('')).toBeFalsy();
  });
});

describe('addTesters', () => {
  let core: Core;

  beforeEach(() => {
    core = new Core();
  });

  test('adding no tester', () => {
    const action = () => core.addTesters();

    expect(action).not.toThrow();
  });

  test('adds a single tester', () => {
    const action = () =>
      core.addTesters(_ => {
        return;
      });

    expect(action).not.toThrow();
  });

  test('adds multiple testers', () => {
    const action = () =>
      core.addTesters(
        _ => {
          return;
        },
        (_, locale) => {
          return locale;
        },
        _ => {
          return;
        },
      );

    expect(action).not.toThrow();
  });

  test('adding duplicate tester is ignored', () => {
    const tester = jest.fn((value, locale) => 'error');

    core.addTesters(tester);
    core.validate('');

    expect(core.getErrors()).toHaveLength(1);
    expect(tester).toBeCalledTimes(1);

    // Add tester again
    core.addTesters(tester);
    core.validate('');

    expect(core.getErrors()).toHaveLength(1);
    expect(tester).toBeCalledTimes(2);
  });
});

describe('removeTesters', () => {
  let core: Core;
  let tester: ITester;

  beforeEach(() => {
    tester = jest.fn((_: any) => 'error');
    core = new Core();
    core.addTesters(tester);
    core.validate('');
  });

  test('removing a tester succeeds', () => {
    expect(core.getErrors()).toHaveLength(1);
    expect(tester).toBeCalledTimes(1);

    core.removeTesters(tester);
    core.validate('');

    expect(core.getErrors()).toHaveLength(0);
    expect(tester).toBeCalledTimes(1);
  });

  test('removing a that was not attached does nothing', () => {
    expect(core.getErrors()).toHaveLength(1);
    expect(tester).toBeCalledTimes(1);

    core.removeTesters(_ => 'other');
    core.validate('');

    expect(core.getErrors()).toHaveLength(1);
    expect(tester).toBeCalledTimes(2);
  });
});

describe('removeAllTesters', () => {
  let core: Core;
  let tester: ITester;

  beforeEach(() => {
    tester = jest.fn((value, locale) => {
      return;
    });
    core = new Core(tester, _ => 'error', _ => ['error1', 'error2']);
    core.validate('');
  });

  test('removes all the testers', () => {
    expect(core.getErrors()).toHaveLength(3);
    expect(tester).toBeCalledTimes(1);

    core.removeAllTesters();

    core.validate('');
    expect(core.getErrors()).toHaveLength(0);
    expect(tester).toBeCalledTimes(1);
  });
});

describe('getErrors', () => {
  let core: Core;

  beforeEach(() => {
    core = new Core();
  });

  test('is empty before validate() is called', () => {
    expect(core.getErrors()).toHaveLength(0);

    core.addTesters(_ => {
      return;
    });
    expect(core.getErrors()).toHaveLength(0);
  });

  test('is empty when validate() found no errors', () => {
    core.addTesters(_ => {
      return;
    });
    core.validate('');

    expect(core.getErrors()).toHaveLength(0);
  });

  test('to have error from failed testers', () => {
    core.addTesters(_ => 'Dummy Error 1', _ => ['Dummy Error 2', 'Dummy Error 3']);
    core.validate('');

    const errors = core.getErrors();

    expect(errors).toHaveLength(3);
    expect(errors).toEqual(['Dummy Error 1', 'Dummy Error 2', 'Dummy Error 3']);
  });
});

describe('setLocale', () => {
  test('changes the locale passed to testers', () => {
    const tester: ITester = jest.fn((value, locale) => 'error');
    const core: Core = new Core(tester);

    expect(core.validate('')).toBeFalsy();
    expect(tester).toBeCalledTimes(1);
    expect(tester).toBeCalledWith('', 'en-US');
    expect(tester).not.toBeCalledWith('', 'en-ZA');

    core.setLocale('en-ZA');

    expect(core.validate('')).toBeFalsy();
    expect(tester).toBeCalledTimes(2);
    expect(tester).toBeCalledWith('', 'en-ZA');
  });
});
