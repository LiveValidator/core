import { ITester } from './abstractions';

/**
 * Core that is responsible for holding a set of testers
 */
export class Core {
  private errors: string[];
  private testers: ITester[];
  private locale: string = 'en-US';

  /**
   * Construct core with testers
   * @param   testers  Set of tester this core will use for validation
   */
  public constructor(...testers: ITester[]) {
    this.testers = [];
    this.errors = [];

    this.addTesters(...testers);
  }

  /**
   * Set the locale that will be passed to testers
   * @param locale The locale to set
   */
  public setLocale(locale: string) {
    this.locale = locale;
  }

  /**
   * Validates a single value against this core's testers
   * @param value The value to validate
   * @returns     True if the value passes
   */
  public validate(value: any): boolean {
    // Clear the current errors
    // We set the length to 0 because others might have a reference to them which we do not want to dangle
    this.errors.length = 0;

    for (const tester of this.testers) {
      const result = tester(value, this.locale);

      if (result instanceof Array) {
        this.errors.push(...result);
      } else if ('string' === typeof result) {
        this.errors.push(result);
      }
    }

    return 0 === this.errors.length;
  }

  /**
   * Gets all the errors of the last [[validate]] run
   * @returns An array of errors
   */
  public getErrors(): string[] {
    return this.errors;
  }

  /**
   * Allows the adding of new testers. Duplicates that are already
   * added will be ignored.
   * @param testersAdd Testers to add
   */
  public addTesters(...testersAdd: ITester[]) {
    const testersNew = testersAdd.filter(testerAdd => -1 === this.testers.indexOf(testerAdd));

    this.testers.push(...testersNew);
  }

  /**
   * Allows the removing of testers attached to core.
   * @param testersRemove Testers to remove
   */
  public removeTesters(...testersRemove: ITester[]) {
    this.testers = this.testers.filter(tester => -1 === testersRemove.indexOf(tester));
  }

  /**
   * Removes all the testers attached to core
   */
  public removeAllTesters() {
    this.testers = [];
  }
}
