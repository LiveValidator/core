/**
 * Class to handle the storage and translation of errors
 */
export class Translator {
  /**
   * Adds an error for a specific locale to the translator
   * @param key Key associated with the error
   * @param message The error message
   * @param locale Locale for the message
   */
  public static addError(key: string, message: string, locale: string = 'en-US') {
    // Check if the locale exists
    if (!(locale in this.errors)) {
      this.errors[locale] = {};
    }

    this.errors[locale][key] = message;
  }

  /**
   * Get an error in a specific locale
   * @param key The key of the error to get
   * @param locale The locale of the error to get
   * @returns     The error formatted in the specified locale
   */
  public static getError(key: string, locale: string = 'en-US'): string {
    // Default to en-US if locale is not found
    if (!(locale in this.errors)) {
      locale = 'en-US';
    }

    // Default to default if key is not found
    if (!(key in this.errors[locale])) {
      key = 'default';
    }

    return this.errors[locale][key];
  }

  private static errors: { [id: string]: { [id: string]: string } } = {
    'en-US': {
      default: 'An unspecified error occurred',
    },
  };
}
